<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 0px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 2px;
  padding-bottom: 2px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
</head>
<body>

<h1>Data Siswa</h1>

<table id="customers">
  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Jenis Kelamin</th>
    <th>Tetala</th>
    <th>Alamat</th>
    <th>Kecamatan</th>
    <th>Kabupaten</th>
    <th>No Telpon</th>
    <th>Asal Sekolah</th>
    <th>Jurusan</th>
  </tr>
  @php
      $no=1;
  @endphp
  @foreach ($data as $row)
  <tr>
  <td>{{ $no++ }}</td>
     <td>{{$row->nama}}</td>
     <td>{{$row->jeniskelamin}}</td>
     <td>{{$row->tetala}}</td>
     <td>{{$row->alamat}}</td>
     <td>{{$row->kecamatan}}</td>
     <td>{{$row->kabupaten}}</td>
     <td>{{$row->notelpon}}</td>
     <td>{{$row->asalsekolah}}</td>
     <td>0{{$row->jurusan}}</td>
  </tr>
  @endforeach
  
  
</table>

</body>
</html>