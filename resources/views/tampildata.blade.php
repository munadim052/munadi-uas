@extends('layout.admin')

@section('content')

<body>
    <h1 class="text-center mb-4">Edit Data Siswa</h1>

    <div class="container">
    
       <div class="row justify-content-center">
        <div class="col-8">
        <div class="card">
              <div class="card-bady">
              <form action="/updatedata/{{ $data->id }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                      <input type="text"name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nama }}">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Jenis Kelamin</label>
                      <select class="form-select"name="jeniskelamin" aria-label="Default select example">
                          <option selected>{{ $data->jeniskelamin }}</option>
                          <option value="cowo">cowo</option>
                          <option value="cewe">cewe</option>
                        </select>
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Tetala</label>
                      <input type="text"name="tetala" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"value="{{ $data->tetala }}">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Alamat</label>
                      <input type="text"name="alamat" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"value="{{ $data->alamat}}">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Kecamatan</label>
                      <select class="form-select"name="kecamatan" aria-label="Default select example">
                          <option selected>{{ $data->kecamatan }}</option>
                          <option value="camplong">Camplong</option>
                          <option value="torjun">Torjun</option>
                          <option value="omben">Omben</option>
                          <option value="tlanakan">Tlanakan</option>
                          <option value="larangan">Larangan</option>
                        </select>
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Kabupaten</label>
                      <select class="form-select"name="kabupaten" aria-label="Default select example">
                          <option selected>{{ $data->kabupaten }}</option>
                          <option value="sampang">Sampang</option>
                          <option value="pemekasan">Pamekasan</option>
                         
                        </select>
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">No Telpon</label>
                      <input type="number"name="notelpon" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"value="{{ $data->notelpon }}">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Asal Sekolah</label>
                      <input type="text"name="asalsekolah" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"value="{{ $data->asalsekolah }}">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Jurusan</label>
                      <select class="form-select"name="jurusan" aria-label="Default select example">
                          <option selected>Pilih Jurusan</option>
                          <option value="Multimedia">{{ $data->jurusan }}</option>
                          <option value="Teknik Komputer Dan Jaringan">Teknik Komputer Dan Jaringan</option>
                          <option value="Tataboga">Tataboga</option>
                          <option value="Tatarias">Tatarias</option>
                        </select>
                    </div>
                    
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
      
       </div>
    </div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
  @endsection