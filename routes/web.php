<?php

use App\Models\Siswa;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $jumlahsiswa = Siswa::count();

    $jumlahsiswacowo = Siswa::where('jeniskelamin','cowo')->count();
    
    $jumlahsiswacewe = Siswa::where('jeniskelamin','cewe')->count();

    return view('welcome',compact('jumlahsiswa','jumlahsiswacowo','jumlahsiswacewe'));
})->middleware('auth');


Route::get('/pendaftaransiswabaru',[SiswaController::class, 'index'])->name('pendaftaransiswabaru')->middleware('auth');

Route::get('/tambahsiswa',[SiswaController::class, 'tambahsiswa'])->name('tambahsiswa');
Route::post('/insertdata',[SiswaController::class, 'insertdata'])->name('insertdata');

Route::get('/tampilkandata/{id}',[SiswaController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[SiswaController::class, 'updatedata'])->name('updatedata');

Route::get('/delete/{id}',[SiswaController::class, 'delete'])->name('delete');

//Export PDF
Route::get('/exportpdf',[SiswaController::class, 'exportpdf'])->name('exportpdf');

//Export PDF
Route::get('/exportexcel',[SiswaController::class, 'exportexcel'])->name('exportexcel');


Route::post('/importexcel',[SiswaController::class, 'importexcel'])->name('importexcel');

Route::get('/login',[LoginController::class, 'login'])->name('login');
Route::post('/loginproses',[LoginController::class, 'loginproses'])->name('loginproses');


Route::get('/register',[LoginController::class, 'register'])->name('register');
Route::post('/registeruser',[LoginController::class, 'registeruser'])->name('registeruser');

Route::get('/logout',[LoginController::class, 'logout'])->name('logout');