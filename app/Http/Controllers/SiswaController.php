<?php

namespace App\Http\Controllers;


use PDF;
use App\models\Siswa;
use App\Exports\SiswaExport;
use App\Imports\SiswaImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;



class SiswaController extends Controller
{
    public function index(Request $request){

        if($request->has('search')){
            $data = Siswa::where('nama','LIKE','%' .$request->search.'%')->Paginate(5);
        }else{
            $data = Siswa::Paginate(5);
        }

        return view('datasiswa',compact('data'));
    }

    public function tambahsiswa(){
        return view ('tambahdata');
    }

    public function insertdata(Request $request){
        //dd($request->all());
        $data = Siswa::create($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('fotosiswa/', $request->file('foto')->getClientoriginalName());
            $data->foto = $request->file('foto')->getClientoriginalName();
            $data->save();
        }
        return Redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Ditambah');
    }

    public function tampilkandata($id){

        $data = Siswa::find($id);
        //dd($data);
        return view('tampildata', compact('data'));
    }

    public function updatedata(Request $request, $id){
        $data = Siswa::find($id);
        $data->update($request->all());
        return Redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Update');
    }

    public function delete($id){
        $data = Siswa::find($id);
        $data->delete();
        return Redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Dihapus');
    }

    public function exportpdf(){
        $data = Siswa::all();

        view()->share('data', $data);
        $pdf = PDF::loadview('datasiswa-pdf');
        return $pdf->download('data.pdf');
    }

    public function exportexcel(){
        return Excel::download(new SiswaExport, 'datasiswa.xlsx');
    }

    public function importexcel(Request $request){
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('SiswaData', $namafile);

        Excel::import(new SiswaImport, \public_path('/SiswaData/'.$namafile));
        return \redirect()->back();
    }
}
