<?php

namespace App\Imports;

use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;

class SiswaImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Siswa([
            'nama' => $row[1],
            'jeniskelamin' => $row[2],
            'tetala' => $row[3],
            'alamat' => $row[4],
            'kecamatan' => $row[5],
            'kabupaten' => $row[6],
            'notelpon' => $row[7],
            'asalsekolah' => $row[8],
            'jurusan' => $row[9],
            'foto' => $row[10]
        ]);
    }
}
