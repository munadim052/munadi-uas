<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->enum('jeniskelamin',['cowo','cewe']);
            $table->text('tetala');
            $table->string('alamat');
            $table->enum('kecamatan',['camplong','omben','torjun','tlanakan','larangan']);
            $table->enum('kabupaten',['sampang','pamekasan']);
            $table->bigInteger('notelpon');
            $table->string('asalsekolah');
            $table->enum('jurusan',['multimedia','teknikkomputerdanjaringan','tataboga','tatarias']);
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
