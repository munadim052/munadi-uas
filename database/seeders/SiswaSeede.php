<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiswaSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswas')->insert([
            'nama' => 'munadi',
            'jeniskelamin' => 'cowo',
            'tetala' => 'sampang',
            'alamat' => 'rabasan',
            'kecamatan' => 'camplong',
            'kabupaten' => 'sampang',
            'notelpon' => '087727657902',
            'asalsekolah' => 'smpi nurul huda',
            'jurusan' => 'multimedia',
        ]);
    }
}
